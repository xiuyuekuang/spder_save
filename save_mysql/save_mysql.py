#-*-coding:utf-8 -*-
import sys
import MySQLdb
import ConfigParser
import os
reload(sys)
sys.setdefaultencoding("utf-8")


class SaveMysql(object):
    '''保存数据到mysql数据库'''
    def __init__(self):
        '''在配置文件中读取参数'''
        config = ConfigParser.ConfigParser()
        with open('database.conf', 'r') as cfgfile:
            config.readfp(cfgfile)
        host = config.get("database", 'HOST')
        db = config.get("database", 'DB')
        passwd = config.get("database", 'PWD')
        charset = config.get("database", 'CHARST')
        user = config.get("database", 'USRE')
        use_unicode = config.get("database", 'USE_UNICODE')


        self.db = MySQLdb.connect(host=host, db=db, passwd=passwd, charset=charset, user=user, use_unicode=use_unicode)
        # self.db = MySQLdb.connect(host='localhost', db='books', passwd='123456', charset='utf8', user='root', use_unicode=True)
        print '数据库已连接...'
        self.cursor = self.db.cursor()
        sql = "create table if not exists b(id INTEGER PRIMARY KEY auto_increment NOT NULL UNIQUE , `name` VARCHAR (20) NOT NULL , age VARCHAR (10) NOT NULL , sex VARCHAR (1) NOT NULL )"
        self.cursor.execute(sql)
        print '创建表!'

    def save(self, *args):
        sql = "insert into....."
        self.cursor.execute(sql)
        self.db.commit()

    def del_table(self):
        sql = "drop table if EXISTS b"
        self.cursor.execute(sql)
        print '数据表, 已删除 !'

    def __del__(self):
        self.cursor.close()
        self.db.cursor()
        print 'Mysql已关闭!'

if __name__ == '__main__':
    sa = SaveMysql()
    sa.del_table()
