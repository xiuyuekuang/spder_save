#-*-coding:utf-8 -*-
import sys
from get_html import get_html
from parse_message import parse_message
from save_mysql import save_mysql
from save_redis import save_redis
from save_sql import save_sql
from save_csv import save_csv
reload(sys)
sys.setdefaultencoding("utf-8")


def main(url):
    sa = save_csv.SaveCsv()
    Response = get_html.gethtml(url=url)
    response = Response()
    response =  response.response
    a_list = response.xpath("//div[@class='entrylistItem']/div[@class='entrylistPosttitle']/a")
    href_list = []
    title_list = []
    for a in a_list:
        href = a.xpath("@href")[0]
        title = a.xpath("text()")[0]
        href_list.append(href)
        title_list.append(title)
        # print etree.tostring(href, pretty_print=True)
    href_list, title_list = sorted(href_list, reverse=True), sorted(title_list, reverse=True)
    sa.save((href_list, title_list))

if __name__ == '__main__':
    url = "http://www.cnblogs.com/adc8868/category/829451.html"
    main(url)
    # a = [5,4,3,2,1]
    # b = sorted(a, reverse=True)
    # print b, '*'*10