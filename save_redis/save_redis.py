#-*-coding:utf-8 -*-
import sys
import redis
import ConfigParser
reload(sys)
sys.setdefaultencoding("utf-8")


class SaveRedis(object):

    def __init__(self):
        '''连接redis'''
        config = ConfigParser.ConfigParser()
        with open('redisdb.conf', 'r') as cfgfile:
            config.readfp(cfgfile)
        host = config.get("redisdb", 'HOST')
        port = config.get("redisdb", 'PORT')
        db = config.get("redisdb", 'DB')

        self.handler = redis.Redis(host=host, port=port, db=db)
        print 'Redis已连接...'

    def save(self, *args):
        self.handler.set('name', args, ex=10)
        print '保存 OK'

    def read(self):
        name = self.handler.get(name='name')
        print name
if __name__ == '__main__':
    sa = SaveRedis()
    sa.save(1,2,3,4,5)
    sa.read()