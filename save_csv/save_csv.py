#-*-coding:utf-8 -*-
import sys
import csv
import pandas as pd
reload(sys)
sys.setdefaultencoding("utf-8")

class SaveCsv(object):
    def __init__(self):
        '''创建列命, 写入列命'''
        # name = ('name', 'age', 'sex')
        self.file_name = 'test.csv'

    def save(self, *args):
        args =  args[0]
        name = ['href', 'title']
        a = dict(x for x in zip(name, args))

        datafram = pd.DataFrame(a)
        datafram.to_csv('Python.csv', index=False, mode='a')

    def readcsv(self, file_name):
        data = pd.read_csv(file_name)
        return data

if __name__ == '__main__':
    sa = SaveCsv()
    # sa.save(1, 2, 3)
    sa.save([1, 2, 3], [1, 2, 3], [1, 2, 3])
    sa.readcsv('test.csv')
    # name = ['name', 'age', 'sex']
    # args = 1, 2, 3
    # a = dict(x for x in zip(name, args))
    # print a