#-*-coding:utf-8 -*-
import sys
import urllib2
from fake_useragent import UserAgent
import cookielib
from lxml import etree
reload(sys)
sys.setdefaultencoding("utf-8")


def gethtml(url, num_tries=2, proxySwich=True):
    '''
    此函数主要为获取网页, 序列化xml格式, 具有代理ip, cookie登录等功能....
    :param url: url地址
    :param num_tries: 尝试次数
    :param proxySwich: ip代理开关
    :return:
    '''
    print proxySwich, '=============='
    ua = UserAgent()
    headers = {
        "User-Agent": ua.random,
    }
    cookie = cookielib.CookieJar()
    cookie_handler = urllib2.HTTPCookieProcessor(cookie)
    # httpproxy_handler = urllib2.ProxyHandler({'http': '110.73.10.80:8123'})
    try:
        if proxySwich:
            httpproxy_handler = urllib2.ProxyHandler({'http': '124.88.67.81:80'})
        else:
            httpproxy_handler = urllib2.ProxyHandler({})
        http_handler = urllib2.HTTPHandler(debuglevel=1)
        https_handler = urllib2.HTTPSHandler(debuglevel=1)
        opener = urllib2.build_opener(http_handler, https_handler, httpproxy_handler, cookie_handler)
        request = urllib2.Request(url, headers=headers)
        html = opener.open(request).read()
        xlm = etree.HTML(html)
    except urllib2.URLError as e:
        proxySwich = False
        if num_tries > 0:
            print '请求失败....尝试第%s次' % num_tries
            return gethtml(url, num_tries-1, proxySwich)
        xlm = None
    Response = type('Response', (), {'url': url, 'response': xlm})
    return Response
if __name__ == '__main__':
    # url = "http://httpstat.us/500"
    # url = "http://www.baidu.com"
    url = "https://www.zhihu.com/explore"
    Response = gethtml(url)
    response = Response()
    print response.url, response.response